#!/bin/bash
echo "HOLA"
filename="version"
while read -r line; do
    echo "$line"
    #VAMOS A TAGUEAR CONSTRUIR LAS IMAGES Y A TAGUEARLAS

    docker build -t wygd/ayd2:backend${line} ./Backend
    docker build -t wygd/ayd2:frontend${line} ./Frontend
    # la siguiente intruccion es para loguearse a docker hub, teniendo el passwrod en un archivo
    # tambien se podria hacer proporcionando una variable de entorno.
    cat /home/ubuntu/passdockerhub | docker login --username wygd --password-stdin
    docker push wygd/ayd2:backend${line}
    docker push wygd/ayd2:frontend${line}
    echo "FIN BUILD-SCRIPT"
done < "$filename"
