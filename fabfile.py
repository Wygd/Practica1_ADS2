from fabric import task,Connection
@task
def deploy(ctx):
        print("Deploy a 52.90.252.63!")
        c = Connection(host='52.90.252.63',user='ubuntu',port='22',connect_kwargs={'key_filename':'/home/ubuntu/.ssh/id_rsa',},)
        result = c.put('./.env')
        result = c.put('./docker-compose.yml')
        result = c.run('docker-compose up -d')
